import Vue from 'vue';
import App from './App.vue';
import Router from 'vue-router';
import Home from './Home.vue';
import CopmanyOkrList from './CompanyOkrList.vue';
import TeamOkrs from './TeamOkrs.vue';
import PersonOkrs from './PersonOkrs.vue';
import BootstrapVue from 'bootstrap-vue';

Vue.use(BootstrapVue);
Vue.use(Router);

const routes = [
    {
        path: '/home',
        component: Home,
    },
    {
        path: '/company-okrs',
        component: CopmanyOkrList,
    },
    {
        path: '/team/:id',
        component: TeamOkrs,
    },
    {
        path: '/person/:id',
        component: PersonOkrs,
    },
];

const router = new Router({
    mode: 'hash',
    routes
});

new Vue({
    render: h => h(App),
    router,
}).$mount('#app');
