const webpack = require('webpack');

const babelOptions = {
    babelrc: false,
    presets: ["es2015", "stage-2"],
    plugins: ["transform-object-rest-spread"],
};

module.exports = {
    entry: './main.js',
    output: {
        filename: 'okr_tool.js',
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        js: `babel-loader?${JSON.stringify(babelOptions)}`,
                    },
                },
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                options: babelOptions,
            },
        ],
    },
    devServer: {
        hot: true,
        inline: true,
        host: 'localhost',
        port: 7999,
        disableHostCheck: true,
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
    ],
    devtool: 'eval',
}
