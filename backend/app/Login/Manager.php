<?php
declare(strict_types=1);

namespace App\Login;

class Manager
{
	private $accessToken;

	public function __construct(AccessToken $accessToken)
	{
		$this->accessToken = $accessToken;
	}

	public function hsaAccessToken(User $user): bool
	{
		return is_null($user->getAccessToken());
	}

	public function generateAccessToken(User $user): User
	{
		$accessToken  = $this->accessToken->create($user);
		$user->setAccessToken($accessToken);
		return $user;
	}
}
