<?php
declare(strict_types=1);

namespace App\Login;

class AccessToken
{
	public function create (User $user): string {
		return sha1($user->getCreatedTimestamp());
	}
}
