<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 14 Oct 2017 15:35:39 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KeyResult
 * 
 * @property int $id
 * @property string $name
 * @property int $objective_id
 * @property int $start_value
 * @property int $goal_value
 * @property \Carbon\Carbon $created_timestamp
 * 
 * @property \App\Models\Objctive $objctive
 * @property \Illuminate\Database\Eloquent\Collection $key_result_progresses
 *
 * @package App\Models
 */
class KeyResult extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'objective_id' => 'int',
		'start_value' => 'int',
		'goal_value' => 'int'
	];

	protected $dates = [
		'created_timestamp'
	];

	protected $fillable = [
		'name',
		'objective_id',
		'start_value',
		'goal_value',
		'created_timestamp'
	];

	public function objctive()
	{
		return $this->belongsTo(\App\Models\Objctive::class, 'objective_id');
	}

	public function key_result_progresses()
	{
		return $this->hasMany(\App\Models\KeyResultProgress::class);
	}
}
