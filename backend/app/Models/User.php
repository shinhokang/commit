<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 14 Oct 2017 15:35:39 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class User
 * 
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property int $account_id
 * @property \Carbon\Carbon $created_timestamp
 * @property int $node_id
 * 
 * @property \App\Models\Account $account
 * @property \App\Models\Node $node
 * @property \Illuminate\Database\Eloquent\Collection $apps
 * @property \Illuminate\Database\Eloquent\Collection $key_result_progresses
 * @property \Illuminate\Database\Eloquent\Collection $users_4_objectives
 *
 * @package App\Models
 */
class User extends Eloquent
{
	protected $table = 'user';
	public $timestamps = false;

	protected $casts = [
		'account_id' => 'int',
		'node_id' => 'int'
	];

	protected $dates = [
		'created_timestamp'
	];

	protected $fillable = [
		'firstname',
		'lastname',
		'email',
		'account_id',
		'created_timestamp',
		'node_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

	public function node()
	{
		return $this->belongsTo(\App\Models\Node::class);
	}

	public function apps()
	{
		return $this->hasMany(\App\Models\App::class);
	}

	public function key_result_progresses()
	{
		return $this->hasMany(\App\Models\KeyResultProgress::class);
	}

	public function users_4_objectives()
	{
		return $this->hasMany(\App\Models\Users4Objective::class);
	}
}
