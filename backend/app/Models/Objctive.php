<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 14 Oct 2017 15:35:39 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Objctive
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $node_id
 * @property int $main_objective_id
 * @property int $interval_type
 * @property \Carbon\Carbon $start_time
 * @property \Carbon\Carbon $end_time
 * @property \Carbon\Carbon $created_timestamp
 * 
 * @property \App\Models\Node $node
 * @property \Illuminate\Database\Eloquent\Collection $key_results
 * @property \Illuminate\Database\Eloquent\Collection $users_4_objectives
 *
 * @package App\Models
 */
class Objctive extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'node_id' => 'int',
		'main_objective_id' => 'int',
		'interval_type' => 'int'
	];

	protected $dates = [
		'start_time',
		'end_time',
		'created_timestamp'
	];

	protected $fillable = [
		'name',
		'description',
		'node_id',
		'main_objective_id',
		'interval_type',
		'start_time',
		'end_time',
		'created_timestamp'
	];

	public function node()
	{
		return $this->belongsTo(\App\Models\Node::class);
	}

	public function key_results()
	{
		return $this->hasMany(\App\Models\KeyResult::class, 'objective_id');
	}

	public function users_4_objectives()
	{
		return $this->hasMany(\App\Models\Users4Objective::class, 'objective_id');
	}
}
