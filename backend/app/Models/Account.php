<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 14 Oct 2017 15:35:39 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Account
 * 
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_timestamp
 * 
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Account extends Eloquent
{
	protected $table = 'account';
	public $timestamps = false;

	protected $dates = [
		'created_timestamp'
	];

	protected $fillable = [
		'name',
		'created_timestamp'
	];

	public function users()
	{
		return $this->hasMany(\App\Models\User::class);
	}
}
