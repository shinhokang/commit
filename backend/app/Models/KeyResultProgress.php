<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 14 Oct 2017 15:35:39 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KeyResultProgress
 * 
 * @property int $id
 * @property string $description
 * @property int $key_result_id
 * @property int $user_id
 * @property float $current_value
 * @property \Carbon\Carbon $created_timestamp
 * 
 * @property \App\Models\KeyResult $key_result
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class KeyResultProgress extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'key_result_id' => 'int',
		'user_id' => 'int',
		'current_value' => 'float'
	];

	protected $dates = [
		'created_timestamp'
	];

	protected $fillable = [
		'description',
		'key_result_id',
		'user_id',
		'current_value',
		'created_timestamp'
	];

	public function key_result()
	{
		return $this->belongsTo(\App\Models\KeyResult::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
