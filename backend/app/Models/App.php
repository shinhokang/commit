<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 14 Oct 2017 15:35:39 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class App
 * 
 * @property int $id
 * @property int $user_id
 * @property string $access_token
 * @property \Carbon\Carbon $created_timestamp
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class App extends Eloquent
{
	protected $table = 'app';
	public $timestamps = false;

	protected $casts = [
		'user_id' => 'int'
	];

	protected $dates = [
		'created_timestamp'
	];

	protected $hidden = [
		'access_token'
	];

	protected $fillable = [
		'user_id',
		'access_token',
		'created_timestamp'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
