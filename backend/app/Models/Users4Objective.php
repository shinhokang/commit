<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 14 Oct 2017 15:35:39 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Users4Objective
 * 
 * @property int $id
 * @property int $objective_id
 * @property int $user_id
 * 
 * @property \App\Models\Objctive $objctive
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Users4Objective extends Eloquent
{
	protected $table = 'users_4_objective';
	public $timestamps = false;

	protected $casts = [
		'objective_id' => 'int',
		'user_id' => 'int'
	];

	protected $fillable = [
		'objective_id',
		'user_id'
	];

	public function objctive()
	{
		return $this->belongsTo(\App\Models\Objctive::class, 'objective_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
