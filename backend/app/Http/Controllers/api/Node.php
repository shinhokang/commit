<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Node as NodeModel;

class Node extends Controller
{
	public function set(Request $request) {
		$node = new NodeModel();
		$node->name = $request->get('name');
		$node->save();

		return $node;
	}
}
