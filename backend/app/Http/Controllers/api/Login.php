<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\App;

class Login extends Controller
{
    public function login(){
        $email = array_get($_GET,"email",null);
        $user = User::where("email","=",$email)->first();
        if($user == null){
            $user = new User();
            $user->email=$email;
            $user->account_id = 1;
            $user->save();

            $app = new App();
            $app->user_id = $user->id;
            $app->access_token = md5(date("y-m-d h:i:s"));
            $app->save();

        }else{

            $app = App::where("user_id","=",$user->id)->first();
        }

        return json_encode(array("access_token",$app->access_token));


    }
}
