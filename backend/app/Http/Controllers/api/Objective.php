<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Objctive;
use DateTime;
use Illuminate\Http\Request;

class Objective extends Controller
{
	public function set(Request $request)
	{
		$objective = new Objctive();
		$objective->description = $request->get('description');
		$objective->node_id = $request->get('node_id');
		$objective->interval_type = $request->get('type');


		$objective->save();

        return $objective;
	}

	public function list()
	{
		return Objctive::all();
	}

	public function get(Request $request)
	{
		$id = $request->get('id');
		return json_encode(
			[
				'Id'               => 0,
				'Name'             => 'First Objective',
				'Description'      => 'My first Objective',
				'CreatedTimestamp' => (new DateTime())->format('Y.m.d H:i:s'),
				'MainObjective'    => '',
				'Interval'         => 'year',
				'StartTime'        => (new DateTime())->format('Y.m.d H:i:s'),
				'EndTime'          => (new DateTime())->format('Y.m.d H:i:s'),
			]
		);
	}
}
