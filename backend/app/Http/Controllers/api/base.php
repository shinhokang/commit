<?php

namespace App\Http\Controllers\api;

use App\Models\Account;
use App\Models\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

class base extends Controller
{
    public function getUser(){
        $accessToken = array_get($_GET,"access_token",null);
        $app = App::where('access_token','=',$accessToken)->first();
        $user = null;
        if(count($app) > 0){
            $user = User::where('id',$app->user_id)->first();
        }

        return $user;
    }
}
