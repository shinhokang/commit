<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get(
	'/user', function (Request $request) {
	return $request->user();
}
);

Route::middleware(['cors'])->group(
	function () {
		Route::get('/login', 'api\Login@login');

		Route::get('/keyresults', 'api\KeyResult@index');

		Route::get('/objective/list', 'api\Objective@list');
		Route::get('/objective/get', 'api\Objective@get');
		Route::get('/objective/set', 'api\Objective@set');

		Route::get('/node/set', 'api\Node@set');
	}
);

